#include <GL/gl.h>
#include <GL/glext.h>
#include <windows.h>

#include "TSFuncs.hpp"

static bool glUseClampToEdge = true;

BlFunctionDef(void, __stdcall, blglTexParameteri, GLenum, GLenum, GLint);
BlFunctionHookDef(blglTexParameteri);
void __stdcall blglTexParameteriHook(GLenum target, GLenum pname, GLint param)
{
	if (param == GL_CLAMP && glUseClampToEdge)
		param = GL_CLAMP_TO_EDGE;

	blglTexParameteriOriginal(target, pname, param);
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	/* DLL loads before OpenGL, so any sig scanning results in null pointers.
	   Load OpenGL ahead of the game and acquire a pointer instead. */

	HINSTANCE openglDLL = LoadLibraryA("opengl32.dll");
	if (!openglDLL) return false;

	blglTexParameteri = (tsh_blglTexParameteriFnT)GetProcAddress(openglDLL, "glTexParameteri");
	if (!blglTexParameteri) return false;

	/* Do not call FreeLibrary, otherwise ref count becomes 0 and OpenGL
	   is unloaded, causing pointer to become invalid.  The OS will handle
	   it if the DLL is ejected. */

	BlCreateHook(blglTexParameteri);
	BlTestEnableHook(blglTexParameteri);

	tsf_AddVar("pref::OpenGL::UseGLClampToEdge", &glUseClampToEdge);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlTestDisableHook(blglTexParameteri);
	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
