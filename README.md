# Texture Bleed
This DLL fixes the issue with textures experiencing bleed at their edges.  It does so by forcing Blockland to use GL_CLAMP_TO_EDGE instead of GL_CLAMP when handling textures.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

The DLL provides the variable `$pref::OpenGL::UseGLClampToEdge` which allows you to toggle the functionality at runtime.

## Examples
The example below showcases the issue this DLL fixes.  The left image shows the texture bleeding to black which is unwanted.  The right image shows the texture without bleeding.
Normal Behavior            |  With DLL
:-------------------------:|:-------------------------:
![Texture Bleed](https://i.imgur.com/FjQpthK.png)  |  ![No Texture Bleed](https://i.imgur.com/whwOFhx.png)

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
